FROM ubuntu:18.04

# set display port to avoid crash
ENV DISPLAY=:.99

COPY . /code/

RUN apt-get update && apt-get install -y build-essential && apt-get install --no-install-recommends -y gnumeric && apt-get install -y dbus-x11

CMD ["sh", "code/script.sh"]
